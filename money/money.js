class Money {
    constructor(amount, currency) {
        this.amount = amount;
        this.currency = currency
    }

    static dollar(amount) {
        return new Money(amount, 'USD')
    }

    static franc(amount) {
        return new Money(amount, 'CHF')
    }

    plus(addend) {
        if (this.currency != addend.currency) return null;
        return new Money(this.amount + addend.amount, this.currency)
    }

    times(multiplier) {
        return new Money(this.amount * multiplier, this.currency)
    }

    reduce(bank, to) {
        let rate = bank.rate(this.currency, to);
        return new Money(this.amount / rate, to)
    }
}

module.exports = Money