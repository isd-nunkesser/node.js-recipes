class Bank {
    constructor() {
        this.rates = {}
    }

    addRate(from, to, rate) {
        this.rates[[from, to]] = rate
    }

    rate(from, to) {
        if (from == to) return 1;
        return this.rates[[from, to]]
    }

    reduce(source, to) {
        return source.reduce(this, to)
    }

}

module.exports = Bank