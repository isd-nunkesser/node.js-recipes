const assert = require('assert');
const Money = require('../money');
const Bank = require('../bank');

suite('Money Example', function () {
    suite('Money', function () {
        test('Dollar Multiplication', function () {
            let five = Money.dollar(5);
            assert.deepStrictEqual(five.times(2), Money.dollar(10))
        });

        test('Equality', function () {
            assert.deepStrictEqual(Money.dollar(5), Money.dollar(5));
            assert.notDeepStrictEqual(Money.dollar(5), Money.dollar(6));
            assert.notDeepStrictEqual(Money.franc(5), Money.dollar(5))
        });

        test('Currency', function () {
            assert.equal(Money.dollar(1).currency, "USD");
            assert.equal(Money.franc(1).currency, "CHF")
        })

    });

    suite('Bank', function () {
        test('IdentityRate', function () {
            let bank = new Bank();
            assert.equal(bank.rate('USD', 'USD'), 1)
        });

        test('Reduce Money', function () {
            let bank = new Bank();
            let result = bank.reduce(Money.dollar(1), 'USD');
            assert.deepStrictEqual(result, Money.dollar(1))
        });

        test('Simple Addition', function () {
            let five = Money.dollar(5);
            let ten = five.plus(five);
            let bank = new Bank();
            let result = bank.reduce(ten, 'USD');
            assert.deepStrictEqual(result, Money.dollar(10))
        });

        test('Reduce Money Different Currency', function () {
            let bank = new Bank();
            bank.addRate('CHF', 'USD', 2);
            let result = bank.reduce(Money.franc(2), 'USD');
            assert.deepStrictEqual(result, Money.dollar(1))
        })

    })
});