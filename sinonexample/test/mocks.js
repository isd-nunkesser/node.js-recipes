const sinon = require('sinon');
const assert = require("assert");
const myAPI = require('../myapi');

it('Returns the return value from the original function', function () {
    const mock = sinon.mock(myAPI);
    mock.expects('method').once().returns(42);
    assert.equal(myAPI.method(), 42);
    mock.verify();
});

it('Test should call a method with exceptions', function () {
    const mock = sinon.mock(myAPI);
    mock.expects('method').once().throws();
    assert.throws(function() {
        myAPI.method();
    });
    mock.verify();
});
