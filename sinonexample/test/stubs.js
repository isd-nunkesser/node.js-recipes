const sinon = require('sinon');
const assert = require("assert");
const once = require('../once');

it("returns the return value from the original function", function () {
    var callback = sinon.fake.returns(42);
    var proxy = once(callback);

    assert.equal(proxy(), 42);
});

